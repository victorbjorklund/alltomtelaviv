---
layout: post
title: "Är det säkert att åka till Tel Aviv?"
date: 2015-07-06
backgrounds:
   
thumb: https://farm8.staticflickr.com/7113/7716282676_7905918878_b.jpg
category: 
tags: Jaffa Tel Aviv Mat 
---

## Är det säkert att åka till Tel Aviv?

Det kort svaret är att ja det är säkert att åka till Tel Aviv.
Den bild som vi ofta får av media i Sverige är att Israel är ett oroligt land som ständigt är i krig och dagligen drabbas utav terrorism. Den bilden stämmer inte direkt med verkligheten och särskilt inte för Tel Aviv. Det är förmodligen en dålig idé att bege sig till Gaza eller resa helt ensam på Västbanken men för det mesta når oroligheterna aldrig Tel Aviv. 
Det är visserligen så att även Tel Aviv och Jerusalem då och då drabbas av oroligheter men risken för att drabbas av en terrorattack personligen är mycket liten. Det är större sannolikhet att du drabbas av en fysisk skada när du besöker London än när du besöker Tel Aviv. 
Det som kan upplevas som obehagligt är att under vissa perioder av oroligheter så skickar terrorister i Gaza raketer mot civila i Tel Aviv. Dessa skadar dock sällan någon då de oftast oskadliggörs av den israeliska militären innan de landar men det kan innebär att flyglarmet går och man blir tvungen att bege sig till ett skyddsrum. Med detta sagt så har det aldrig hänt när jag har varit där och förhoppningsvis är chansen liten för att det inträffar under din semester. 