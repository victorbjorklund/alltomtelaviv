---
layout: default
title: "Yoga i Tel Aviv"
date: 2015-05-07
summary: "This is a summary"

backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: https://farm6.staticflickr.com/5042/5295220541_2cbd5eef19_z.jpg
category: 
tags: Tel Aviv Yoga
---

Om du gillar Yoga så kommer du att älska Tel Aviv. Det är väldigt vanligt att se Israeler utföra häppnadsväckande yoga positioner på straden och staden är fylld med Yoga studior. Vad beror det på? Kanske är det att israeler i stort är ett folk som söker mening i livet och att för många kan yoga bidra till det eller är det kanske för att många unga israeler åker till Indien efter att de har avslutat sin militärtjänst. Oavsett anledningen så är yoga populärt i Tel Aviv.

Du kan naturligtvis göra dina yoga övningar på straden om du vill och avsluta med ett dopp i medelhavet men det finns också möjlighet att besöka någon av de många yoga studios som finns runtom i Tel Aviv.

## Ella Yoga

Ella Yoga är en yoga studio som ligger i Tel Aviv's hamn så du kan känna doften av havet i luften. Ella Yoga har en stor bredd på sina klasser som riktar sig från allt till nybörjare till de som är mer erfarna. Det börjar en ny klass nästan varje timme sju dagar i veckan så det är bara att komma förbi. 

*Ella Yoga, Tel Aviv Port, Building 4, Tel Aviv, Israel, +972-3-5444881*

## Studio Naim

Denna yoga studio ligger i hjärtat av Florentine. De som kommer hit är från alla delar av samhället och i alla åldrar så alla kan känna sig välkomna. Studio Naim har en bra balans mellan den meditativa och fysiska aspekten av yoga så efter ett pass här känner man sig utmattad fysiskt samtidigt som man har fått ny energi från den spirutuella kicken. 

*Studio Naim, Derech Shlomo 46, Washington 29, Rothschild 4, Tel Aviv, Israel, +972-3-5188998*

## Prana Yoga College

Ligger på gatan Hovevei Tsiyon St. och är en av Tel Aviv bornas favoritställe för Yoga. Studion omges av en trädgård som i sig verkar lugnande och tar bort fokus från den annars ibland stressiga staden Tel Aviv. Det är en yoga skola som utbildar nya yoga lärare så det kan vara värt ett besök om du är yogalärare idag eller om du har intresse av att bli en yogaläare. Studio erbjuder också helt kostnadsfria yoga klasser då de anser att yoga ska vara tillgängligt för alla människor. 

Studion har endast klasser i Yoga Hatha vilken fokuserar på långsamma rörelser och fokus på andningen. Det kan vara en god idé att vara där tidigt eftersom att klasserna ofta blir fyllda snabbt. 

*Prana Yoga College, 52 Hovevei Tsiyon St., Tel Aviv, Israel, +972-3-6296661*


 