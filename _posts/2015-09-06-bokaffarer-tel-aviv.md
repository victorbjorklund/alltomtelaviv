---
layout: post
title: "Tel Aviv's mysigaste bokaffärer"
date: 2015-09-06
backgrounds: https://farm8.staticflickr.com/7303/13198361573_98db7ab1cc_b.jpg
   
thumb: https://farm3.staticflickr.com/2746/4448968798_00f618c77b_o.jpg
category: 
tags: Tel Aviv 
---

Om du liksom jag tycker älskar böcker och att ströläsa böcker i bokaffären eller biblioteket så kommer du att älska Tel Aviv. Bokaffärerna här handlar inte om att bara sälja så många exemplar av den senaste deckaren utan bokaffärerna här speglar verkligen en genuin kärlek till böcker. Bokaffärerna i Tel Aviv säljer ofta begagnade böcker och blandar fritt modern litteratur med obskyra gamla böcker. 

<h2>Halper's bookstore</h2>

Den mest kända bokaffären är nog Halper's bookstore som säljer begagnade böcker. Det är en affär som är fylld till bredden av böcker där det knappt känns som att man får plats pågrund av hur tätt bokhyllorna står. Affären drivs av Josef Halper (att Halper's bookstore drivs av någon som heter Halper kanske inte är en så stor överaskning) och han har under 25 år byggt upp en rejäl samling böcker där du kan hitta det mesta. 

![Josef Halper och Sarah Halper](https://farm8.staticflickr.com/7303/13198361573_98db7ab1cc_b.jpg)
Här ett foto från butiken med Josef Halper och hans dotter Sarah. 

Adress: 87 allenby street

<h2>M. Pollak Books</h2>

Det här är en riktigt häftig bokhandel som även säljer antika kartor. Det finns böcker och dokument här som är flera hundra år gamla och ett besök här är ett måste för alla bokälskare. Affären har funnits sedan 1899 så bara det är en upplevelse. 

Adress: 36 and 42 King George Street 

<h2>The Little Prince</h2>

Och slutligen mitt absoluta favoritställe i Tel Aviv. Detta är en liten och mysig bokaffär fylld med intressanta böcker och med ett kafé/resturang längst in i butiken. 
![The Little Prince i Tel Aviv](https://farm4.staticflickr.com/3367/4617827615_def2d85e2a_o.jpg)
Det är bara att ta en bok från hyllan i filosofi och beställa in en kopp kaffe och sedan bara förkovra sig i bokens värld. Kan inte rekommendera denna bokaffär nog. 

Adress: 19 King George Street