---
layout: post
title: "Hur dyrt är Tel Aviv?"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: https://farm8.staticflickr.com/7113/7716282676_7905918878_b.jpg
category: 
tags: Kibbutz
---

Tel Aviv är långt ifrån den billigaste staden att leva och bo i! Prisnivån är väl ungefär <!--more--> som i Sverige där vissa saker är dyrt och vissa saker är billiga. Det behöver dock inte bli dyrt om du inte handlar på de allra mest turistaktiga gatorna.  Ett tips är att undvika McDonalds i Tel Aviv då det av någon anledning är väldigt dyrt jämfört med "vanliga" restauranger vilket kan upplevas som konstigt för oss svenskar som är vana att tänka på McDonalds som "billig" snabbmat.