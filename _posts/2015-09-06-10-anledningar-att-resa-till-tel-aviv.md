---
layout: post
title: "10 anledningar till att älska Tel Aviv"
date: 2015-09-06
backgrounds: https://farm9.staticflickr.com/8289/7716417222_a0ef15edb0_o.jpg
   
thumb: https://farm9.staticflickr.com/8289/7716417222_a0ef15edb0_o.jpg
category: 
tags: Tel Aviv 
---

<h2>1. Tel Aviv är litet</h2>

Tel Aviv har visserligen nästan en halv miljon invånare men det är trots det en stad där det känns som att alla känner varandra. Du behöver inte åka buss eller tåg för att ta dig runt i staden utan du kan enkelt ta dig till de flesta platser genom att gå eller genom att hyra en cykel. Jag kan verkligen starkt rekommendera att ni går längs straden från norr till Jaffa i den södra delen eller tvärtom. 

Det finns små och mysiga cafer längs hela vägen och det är bara att stanna till för att ta ett dopp i havet eller besöka marknaden och köpa färsk frukt (riktigt färsk frukt i jämförelse med den vi hittar i Sverige)

<h2>2. Det är en 24 timmars stad</h2>

Du vaknar upp tidigt och beger dig till straden för ett morgondopp, sedan vidare till att äta en brunch på något av de mysiga cafen som öppnar när solen går upp och sedan en promenad längs straden ner till hamnen för att bara njuta av den friska havsluften. Därefter kommer lunchen och du står inför ett matparadis där du kan välja allt från de godaste fallafelna till fräska frukter och grönsaker. Eftermiddagen kanske du spenderar inne i staden på köpcentrument eller i en av de mindre butikerna som säljer allt från kläder och smycken till konstverk. Dagen avslutas med en trevlig middag vid havet. 

Men stadan fortsätter att vara igång och det tar aldrig slut så det är upptill dig när du inte orkar mer. 

<h2>2. En unik och historisk kultur som möter det nya</h2>

<h2>3. Den fantastiska maten</h2>

<h2>4. En spännande musik scen</h2>

<h2>5. Nära till resten av Israel</h2>

<h2>6. De fantastiska människorna</h2>

<h2>7. Shopping </h2>

<h2>8. </h2>

<h2>9. </h2>

<h2>10. </h2>