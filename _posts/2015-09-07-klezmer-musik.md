---
layout: post
title: "Klezmer musik i Tel Aviv"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: https://farm3.staticflickr.com/2071/2255128393_b837148dab_o.jpg
category: 
tags: Tel Aviv Klezmer Musik
---

Klezmer musik är usprungligen judisk folkmusik från östeuropa men spelas idag runtom i hela världen. Den moderna klezmer musiken har även tagit in inspiration från andra musikgrenar från hela världen. Om du uppskattar klezmer musiken så är en resa till Tel Aviv perfekt då det där finns många band som spelar Klezmer musik både på gatan och på olika konserter och festivaler. 

Men även här på hemmaplan kan du hitta klezmer musik om du känner att du inte kan få nog av det när du besöker Israel. Här är en video där den svenska klezmer sångerkskan Sofia Berg-Böhm framför en Klezmer låt i utbildningsradion och därefter blir intervjuad i TV-soffan om musikstilen och hennes väg till den. 

<center><iframe width="420" height="315" src="https://www.youtube.com/embed/WWJEwfKG5JU" frameborder="0" allowfullscreen></iframe></center>

<script async src="//"></script>
<!-- Alltomtelaviv - rektangel -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9663308612826959"
     data-ad-slot="9294507624"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

 