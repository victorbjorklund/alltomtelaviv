---
layout: post
title: "Israelisk mat"
date: 2015-09-06
backgrounds:
   
thumb: https://farm4.staticflickr.com/3102/3227608051_f14d865075_z.jpg?zz=1
category: 
tags: Jaffa Tel Aviv Mat 
---

Israel har en lång kultur av att sätta högt värde på mat.En av mina favoriträtter som jag alltid beställer in så fort jag kommer till Israel är Shashoka som kan beskrivas som ägg knäckta i en tomatsås röra. 

<h2>Resturanger</h2>

![Äter bakelse i Tel Aviv](https://farm9.staticflickr.com/8294/7716566700_2128bd2abe_b.jpg)

Tel Aviv är som sagt en stad som tar maten på största allvar och staden är därför fylld med resturanger som serverar allt från traditionell judisk och israelisk mat till sushi.

<h2>Laga israelisk mat</h2>

Och för dig som älskar att laga mat och har möjligheter att laga mat i ditt boende (t.ex. om du har hyrt en lägenhet genom AirBnB) så finns det goda möjligheter att köpa färska mat på marknaden. Och om du inte har möjlighet att laga mat när du är i Israel så kan du alltid handla kryddor och delikatser på marknaden och sedan ta med dig maten hem till Sverige. 


![Handla mat i Tel Aviv](https://farm9.staticflickr.com/8284/7716391422_99c138a19f_b.jpg)