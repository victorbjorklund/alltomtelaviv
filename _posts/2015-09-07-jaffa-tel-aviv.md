---
layout: post
title: "Utforska Jaffa i Tel Aviv"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: https://farm9.staticflickr.com/8294/7716566700_2128bd2abe_b.jpg
category: 
tags: Jaffa Tel Aviv Mat 
---

Jaffa är en av de äldsta delarna av Tel Aviv och du kan verkligen känna historien i luften och fylld med saker att göra. Du kan ta en taxi till Jaffa eller gå längs Tel Aviv stranden (vilket tar 5-30 min beroende på var du startar). Väl där så rekommenderar jag att ni besöker någon av de trevliga resturanger och kafén som finns i området. 

Jag kan starkt rekommendera att ni besöker Old Jaffa kicthen och beställer in äggröra och en turkisk kaffe. Därefter kan ni utforska stadsdelen och har ni tur så kanske marknaden är öppen där man kan köpa allt från trasig elektronik, souvenirer till fina konstverk, men glöm inte att pruta. 

