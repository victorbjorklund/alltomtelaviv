---
layout: post
title: "Tel Aviv Universitet"
date: 2015-09-04
backgrounds: https://farm8.staticflickr.com/7303/13198361573_98db7ab1cc_b.jpg
   
thumb: https://upload.wikimedia.org/wikipedia/commons/3/3a/Naftali_Building._Tel_Aviv_University.jpg
category: 
tags: Tel Aviv University Utbytesstudier 
---

Tel Aviv University är ett av de ledande universiten i Israel och ligger i norra delen av Tel Aviv. Det är ett spännande universitet med stora grönområden fyllt med studenter från hela världen. Jag har själv studerat juridik vid Universitet och kan starkt rekommendera det. Om du är student vid ett svenskt universitet så är det möjligt att du har möjlighet att åka på en utbytestermin till Tel Aviv och studera vid Tel Aviv University. 

Jaffa ä
