---
layout: post
title: "Herods Restaurang Tel Aviv by the Beach"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: http://images.bokahotell.se/hotels/1000000/20000/19700/19666/19666_120_b.jpg
category: hotell
tags: Herods Hotel Tel Aviv by the Beach Tel Aviv
---

Herods Hotel Tel Aviv ligger centralt i Tel Aviv och ett stenkast från stranden. Hotellet har fem stjärnor så ni kan räkna med exceptionell service. 

Det enda som jag tycker är synd med hotellet är att det inte ingår gratis wifi utan det måste du betala för seperata på hotellet. Det känns ju lite märkligt med tanke på hur vanligt det är nu för tiden att trådlöst internet för det mesta är kostnadsfritt. 

Det är dock i övrigt toppen service och rummen har utsikt över havet så det är ändå ett hotell som det är värt att titta närmare på om ni vill bo centralt.

![Herods Hotel Tel Aviv by the Beach](http://images.bokahotell.se/hotels/1000000/20000/19700/19666/19666_119_b.jpg)

<center><a class="mybtn" href="http://clk.tradedoubler.com/click?p(22891)a(2270282)g(1027220)url(http://www.bokahotell.se/hotell-israel/hotell-tel-aviv/herods-hotel-tel-aviv-by-the-beach/)" title="Boka här" target="_blank">Boka här</a><img src="http://impse.tradedoubler.com/imp?type(inv)g(1027220)a(2270282)" /></center>
