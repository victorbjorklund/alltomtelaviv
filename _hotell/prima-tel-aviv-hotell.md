---
layout: post
title: "Prima Tel Aviv Hotel"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: http://images.bokahotell.se/hotels/1000000/480000/478900/478863/478863_67_b.jpg
category: 
tags: Prima Tel Aviv Hotel
---


Prima Tel Aviv Hotel är ett litet och mysigt hotell med fyra stjärnor som ligger några minuter från stranden. 
![Prima Tel Aviv Hotel](http://images.bokahotell.se/hotels/1000000/480000/478900/478863/478863_83_b.jpg)

<center><a class="mybtn" href="http://clk.tradedoubler.com/click?p(22891)a(2270282)g(1027220)url(http://www.bokahotell.se/hotell-israel/hotell-tel-aviv/prima-tel-aviv-hotel/)" title="Boka här" target="_blank">Boka här</a><img src="http://impse.tradedoubler.com/imp?type(inv)g(1027220)a(2270282)" /></center>
