---
layout: post
title: "Grand Beach Hotel"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: http://images.bokahotell.se/hotels/1000000/30000/25500/25410/25410_51_b.jpg
category: hotell
tags: Grand Beach Hotell
---


Grand Beach Hotel ligger inom gångavstånd till straden samt till Israels vackra Självständighetspark. 

![Grand Beach Hotel](http://images.bokahotell.se/hotels/1000000/30000/25500/25410/25410_52_b.jpg)

<center><a class="mybtn" href="http://clk.tradedoubler.com/click?p(22891)a(2270282)g(1027220)url(http://www.bokahotell.se/hotell-israel/hotell-tel-aviv/grand-beach-hotel/)" title="Boka här" target="_blank">Boka här</a><img src="http://impse.tradedoubler.com/imp?type(inv)g(1027220)a(2270282)" /></center>
