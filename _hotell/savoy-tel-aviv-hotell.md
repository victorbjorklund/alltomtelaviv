---
layout: post
title: "The Savoy Tel-Aviv"
date: 2015-09-07
backgrounds: https://farm6.staticflickr.com/5061/5688059726_341f90ab01_b.jpg
   
thumb: http://images.bokahotell.se/hotels/1000000/480000/478900/478836/478836_46_b.jpg
category: hotell
tags: The Savoy Tel-Aviv
---


The Savoy Tel-Aviv ligger ungefär 50 meter från straden så om du vill njuta av solen på straden eller ta en simtur på morgonen så är detta hotell ett bra val. Hotellet är relativt litet så de har ingen egen restaurang utan endast frukost serveras. Det finns dock gott om restauranger i området så det borde inte vara några problem att hitta något gott att äta. 

<img src="http://images.bokahotell.se/hotels/1000000/480000/478900/478836/478836_12_b.jpg" class="img-responsive">

<center><a class="btn btn-primary" href="http://clk.tradedoubler.com/click?p(22891)a(2270282)g(1027220)url(http://www.bokahotell.se/hotell-israel/hotell-tel-aviv/the-savoy-tel-aviv-sea-side/)" title="Boka här" target="_blank">Boka här</a><img src="http://impse.tradedoubler.com/imp?type(inv)g(1027220)a(2270282)" /></center>
